# *-* coding:utf8 *-*

# from qiniu import Auth, put_file, etag
# import qiniu.config
# #需要填写你的 Access Key 和 Secret Key
# access_key = 'RK5BBRqByqtMBMnkAuKQxbcNOisNVVoqlZR6Tzbd'
# secret_key = '5Fe9ni3bK259t1SM8jwt84xKZT7yy0XqQN6AUvVn'
# #构建鉴权对象
# q = Auth(access_key, secret_key)
# #要上传的空间
# bucket_name = 'yangbin'
# #上传到七牛后保存的文件名
# key = 'my-python-logo.png'
# #生成上传 Token，可以指定过期时间等
# token = q.upload_token(bucket_name, key, 3600)
# #要上传文件的本地路径
# localfile = './1.jpg'
# ret, info = put_file(token, key, localfile)
# print(ret,info)
# assert ret['key'] == key
# assert ret['hash'] == etag(localfile)



import logging

from qiniu import Auth, put_data

# 需要填写你的 Access Key 和 Secret Key
access_key = 'RK5BBRqByqtMBMnkAuKQxbcNOisNVVoqlZR6Tzbd'
secret_key = '5Fe9ni3bK259t1SM8jwt84xKZT7yy0XqQN6AUvVn'

# 要上传的空间
bucket_name = 'yangbin'


def storage(data):
    """七牛云存储上传文件接口"""
    if not data:
        return None
    try:
        # 构建鉴权对象
        q = Auth(access_key, secret_key)

        # 生成上传 Token，可以指定过期时间等
        token = q.upload_token(bucket_name)

        # 上传文件
        ret, info = put_data(token, None, data)

    except Exception as e:
        logging.error(e)
        raise e

    if info and info.status_code != 200:
        raise Exception("上传文件到七牛失败")

    # 返回七牛中保存的图片名，这个图片名也是访问七牛获取图片的路径
    return ret["key"]


if __name__ == '__main__':
    file = input('请输入文件路径')
    with open(file, 'rb') as f:
        storage(f.read())



