# *-* coding:utf8 *-*
import logging
from redis import StrictRedis


class Config(object):
    """工程配置信息"""
    SECRET_KEY = "EjpNVSNQTyGi1VvWECj9TvC/+kq3oujee2kTfQUs8yCM6xX9Yjq52v54g+HVoknA"

    DEBUG = True

    # 数据库的配置信息(数据库密码更改为自己的密码)
    SQLALCHEMY_DATABASE_URI = "mysql://root:mysql@127.0.0.1:3306/information3"
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # 关闭数据跟踪
    # 在请求结束时候，如果指定此配置为 True ，那么 SQLAlchemy 会自动执行一次 db.session.commit()操作
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    # redis配置
    REDIS_HOST = "127.0.0.1"
    REDIS_PORT = 6379

    # flask_session的配置信息
    SESSION_TYPE = "redis"  # 指定 session 保存到 redis 中
    SESSION_USE_SIGNER = True  # 让 cookie 中的 session_id 被加密签名处理
    SESSION_REDIS = StrictRedis(host=REDIS_HOST, port=REDIS_PORT)  # 使用 redis 的实例  指定Session 保存的redis
    SESSION_PERMANENT = False  # 设置session需要过期
    PERMANENT_SESSION_LIFETIME = 86400 * 2  # session 的有效期，单位是秒

    # 设置日志等级
    LOG_LEVEL = logging.DEBUG

class DevelopmentConfig(Config):
    """开发环境下"""
    DEBUG = True

class ProductionConfig(Config):
    """生产环境下"""
    DEBUG = False
    LOG_LEVEL = logging.WARNING

class TestingConfig(Config):
    """单元测试"""
    DEBUG = True
    TESTING = True


config = {
    "development":DevelopmentConfig,
    "production":ProductionConfig,
    "testing":TestingConfig
}


