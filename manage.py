# *-* coding:utf8 *-*

from flask_script import Manager
from flask_migrate import Migrate,MigrateCommand
from info import create_app,db,models

# manage.py是程序启动的入口，只关心启动的相关参数以及内容，不关心具体该
# 如果创建app或者相关业务逻辑

# create_app 类似于工厂方法
from info.models import User

app = create_app("development")

manager = Manager(app) #创建终端命令对象
Migrate(app, db) #将app与db关联
manager.add_command('db',MigrateCommand) #添加迁移命令到manay中


@manager.option("-n","-name",dest="name")
@manager.option("-p","-password",dest="password")
def CreateSuperUser(name,password):

    if not all([name,password]):
        print("参数不足")

    user = User()
    user.nick_name = name
    user.mobile = name
    user.password = password
    user.is_admin = True

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        print(e)

    print("添加成功")


if __name__=='__main__':
    # print(app.url_map)
    manager.run()

